/*
   SHAPESHIFTER DIY OPEN SOURCE DRUM MACHINE
   by FASELUNARE.COM

   https://github.com/faselunare/shapeshifter
   Released under the GPLv3 license.
*/

#include "src/ShapeShifter.h"

ShapeShifter myShapeShifter;

void setup() {
  myShapeShifter.begin();
}

void loop() {
  myShapeShifter.run();
}
