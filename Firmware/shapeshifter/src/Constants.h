/*
   SHAPESHIFTER DIY OPEN SOURCE DRUM MACHINE
   by FASELUNARE.COM

   https://github.com/faselunare/shapeshifter
   Released under the GPLv3 license.

*/

#ifndef CONSTANTS_H
#define CONSTANTS_H

#define NUMBER_OF_TRACKS        4
#define NUMBER_OF_CONTROLS      6
#define NUMBER_OF_SUBPATTERNS      4
#define NUMBER_OF_STEPS         16

#define DEBOUNCING_TIME_STEPS   200

#define BUTTON_DEBOUNCE         3

#define FALLING_EDGE            false
#define RISING_EDGE             true

#define LONG_PRESS_TIME         600
#define DEBOUNCING_TIME         500

#define NO_BUTTON          -1

#define TRACKA_POS          0
#define TRACKB_POS          1
#define TRACKC_POS          2
#define TRACKD_POS          3
#define FN_POS              4
#define PLAY_POS            5

#define NO_PUSH             0
#define STANDARD_PUSH       1
#define LONG_PUSH           2

#define PATTERN_LENGTH_POS  4

#endif
