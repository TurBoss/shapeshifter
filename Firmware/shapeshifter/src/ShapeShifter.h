/*
   SHAPESHIFTER DIY OPEN SOURCE DRUM MACHINE
   by FASELUNARE.COM

   https://github.com/faselunare/shapeshifter
   Released under the GPLv3 license.

*/

#ifndef SHAPESHIFTER_H
#define SHAPESHIFTER_H

#include <Arduino.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>
#include <Bounce2.h>
#include <ArduinoJson.h>

#include "PinAssignment.h"
#include "Constants.h"

//#define DEBUG

struct subPatternStruct {
  uint8_t currentSubpattern;
  uint8_t currentSubpatternLength;
  uint8_t chosenSample;
  uint8_t currentStep;
};

class ShapeShifter {
  public:
    void begin();
    void run();

  private:
    /*Functions*/
    void initAudio();

    void driveAllLEDs(bool OnOffn);

    void updateControlButtons(); 
    int updatePatternButtons(bool loadOrSave = false);
    void updateTiming();
    void updateStep();
    void updateLEDsPattern();

    void writeDual595(uint8_t data1, uint8_t data2);
    void playSample();
    void clearTracksLEDs();

    void selectTracksSamples(int trackNumber);
    
    void setLEDs(uint8_t value, bool single = false);

    unsigned int findDifferences(unsigned int current, unsigned int previous, bool risingFallingn);

    String getBinary16String(unsigned int thisValue);
    unsigned int getPatternFromString(String thisString);

    bool saveJson(int thisFileNumber);
    bool loadJson(int thisFileNumber);

#ifdef DEBUG
    String parseCommand();
    void debug();
#endif

    /*Variables*/
    bool operationDone;
    unsigned long updateTime, startTime;
    
    unsigned int previouspatternButtons;

    unsigned int pattern[NUMBER_OF_TRACKS][NUMBER_OF_SUBPATTERNS];
    subPatternStruct subpatterns[NUMBER_OF_SUBPATTERNS];

    uint8_t currentDrum;

    uint8_t selectSound;

    uint8_t mode;
    uint8_t previousMode;

    uint8_t previousPlay;

    float mixerGain[NUMBER_OF_SUBPATTERNS];

    //potentiometers
    int tempo, tempoPrevious;
    int lowPass, lowPassPrevious;
    int bitCrush, bitCrushPrevious;
};

#endif